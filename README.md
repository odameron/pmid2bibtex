# pmid2bibtex

This is a dirty script for generating a clean bibtex entry from a PubMed identifier.

## Usage

`pmid2bibtex.py <pmid|url>` where `<pmid>` is for example `18475267` (article [Use and misuse of the gene ontology annotations](https://www.ncbi.nlm.nih.gov/pubmed/18475267) by Rhee et al), or `url` is the URL of the PubMed's page for the article.

## Todo
- [x] also accept full URL instead of just the pmid
- [ ] merge `pmid2bibtex.py`and `pmid2txt.py` using an optional second parameter for specifying the output format (and transform `pmid2txt.py` into an alias)
- [x] add DOI to the bibtex entry
- [ ] migration to python3 cf branch `migrationPy3`
- [ ] support for book chapters (PubmedArticleSet, cf https://www.ncbi.nlm.nih.gov/pubmed/31895510?report=xml&format=text) cf branch `bookChapter`
