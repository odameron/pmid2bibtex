#! /usr/bin/env python3


## version 1.15	FIXME	support for book chapters (PubmedArticleSet, cf https://www.ncbi.nlm.nih.gov/pubmed/31895510?report=xml&format=text)
## version 1.14	2020-08-20	fix utf-8 for pages
## version 1.13	2020-07-07	migration to python3
## version 1.12	2020-06-17	handle trailing slash gracefuly
## version 1.11 2020-06-15	fix PMID after PubMed changed its website
## version 1.10	2019-07-26	handles DOI
## version 1.9	2018-07-31	accept both full pubmed URL or PMID as argument instead of just the PMID
## version 1.8	2015-04-10	remove spaces from article identifier
## version 1.7	2011-01-07	four-digit year in identifier
## version 1.6	2010-08-23	gracefully handles the lack of pagination element
## version 1.5	2009-03-01	handles correctly CollectiveName in AuthorList
## version 1.4	2009-03-01	comment number firld when the information is not present
## version 1.3	2009-02-25	fixed indentation for seeAlso
## version 1.2	2009-01-06	fixed missing comma after pmid + fixde computation of articleYear if the <ArticleDate> elt is not present + handling of forename/firstname
## version 1.1	2009-01-05

import urllib 			# python2
import urllib.request	#python3
import sys
from xml.dom.minidom import parse

pubmedURL = "http://www.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?rettype=abstract&retmode=xml&db=pubmed&id="


def camelCase(st):
    # TODO: use the camelize() method from the inflection package
    # http://inflection.readthedocs.org/en/latest/
    output = ''.join(x for x in st.title() if x.isalpha())
    return output[0].lower() + output[1:]

# check if parameter exists and looks like a PMID

if len(sys.argv) != 2:
	print("Usage: " + sys.argv[0] + " PMID")
	sys.exit(1)

pmid = sys.argv[1]
#if pmid.startswith("https://www.ncbi.nlm.nih.gov/pubmed/"):
#	pmid = pmid.replace("https://www.ncbi.nlm.nih.gov/pubmed/", "")
if pmid.startswith("https://pubmed.ncbi.nlm.nih.gov/"):
	pmid = pmid.replace("https://pubmed.ncbi.nlm.nih.gov/", "").rstrip("/")

# retrieve pubmed data
pubmedFile = urllib.request.urlopen(pubmedURL + pmid)
dom = parse(pubmedFile)
#dom = parse("/home/olivier/projects/pmid2bibtex/rhee08useMisuseGO.xml")
for article in dom.getElementsByTagName("PubmedArticle"):
	articleTitle = article.getElementsByTagName("ArticleTitle")[0].childNodes[0].data
	if articleTitle.endswith("."):
		articleTitle = articleTitle[:-1]

	articleDOI = ''
	ArticleIdList = article.getElementsByTagName("ArticleId")
	for currentArticleId in ArticleIdList:
		if "IdType" in currentArticleId.attributes.keys() and currentArticleId.attributes["IdType"].value == "doi":
			articleDOI = "https://doi.org/" + currentArticleId.childNodes[0].data
		
		
	articlePages = ""
	if len(article.getElementsByTagName("Pagination")) > 0:
		eltMedlinePgn = article.getElementsByTagName("Pagination")[0].getElementsByTagName("MedlinePgn")
		if eltMedlinePgn[0].hasChildNodes():
		#if len(eltMedlinePgn) > 0:
			articlePages = eltMedlinePgn[0].childNodes[0].data
			dashPos = articlePages.find("-")
			if dashPos != -1:
				firstPage = articlePages[:dashPos]
				lastPageAbbrev = articlePages[dashPos+1:]
				print("firstPage: " + firstPage)
				print("lastPageAbbrev: " + lastPageAbbrev)
				lastPage = "" + str(firstPage.encode("utf-8"))
				if len(lastPageAbbrev) < len(firstPage):
					lastPage = firstPage[:len(firstPage) - len(lastPageAbbrev)] + lastPageAbbrev
					print("lastPage: " + lastPage)
					articlePages = firstPage + "--" + lastPage
		
	eltArticleDate = article.getElementsByTagName("ArticleDate")
	if len(eltArticleDate) > 0:
		articleYear = eltArticleDate[0].getElementsByTagName("Year")[0].childNodes[0].data
	else:
		eltPubDate = article.getElementsByTagName("PubDate")
		if len(eltPubDate) > 0:
			eltPubDateYear = eltPubDate[0].getElementsByTagName("Year")
			if len(eltPubDateYear) > 0:
				articleYear = eltPubDateYear[0].childNodes[0].data
			else:
				# No Year child of PubDate
				eltPubDateMedlineDate = eltPubDate[0].getElementsByTagName("MedlineDate")
				if len(eltPubDateMedlineDate) > 0:
					articleYear = eltPubDateMedlineDate[0].childNodes[0].data[:4]
				else:
					# Figure out how to get the date from the PubDate elt
					print("Unable to figure out the publication year from the PubDate element")
					sys.exit(2)
		else:
			print("Unable to figure out the date")
			sys.exit(2)
	journalNode = article.getElementsByTagName("Journal")[0]
	journalTitle = journalNode.getElementsByTagName("Title")[0].childNodes[0].data
	print("journalTitle: " + str(journalTitle))
	journalVolume = ""
	if len(journalNode.getElementsByTagName("JournalIssue")[0].getElementsByTagName("Volume")) > 0:
		journalVolume = journalNode.getElementsByTagName("JournalIssue")[0].getElementsByTagName("Volume")[0].childNodes[0].data
	journalNumber = ""
	if len(journalNode.getElementsByTagName("JournalIssue")[0].getElementsByTagName("Issue")) > 0:
		journalNumber = journalNode.getElementsByTagName("JournalIssue")[0].getElementsByTagName("Issue")[0].childNodes[0].data
	
	authors = []
	currentAuthorListList = article.getElementsByTagName("AuthorList")
	if len(currentAuthorListList) > 0:
		for currentAuthorList in currentAuthorListList: # just in case there would be more than one author list for one article... probably never happens
			for currentAuthor in currentAuthorList.getElementsByTagName("Author"):
				if len(currentAuthor.getElementsByTagName("CollectiveName")) > 0:
					currentLastName = currentAuthor.getElementsByTagName("CollectiveName")[0].childNodes[0].data
					currentForeName = ""
				else:
					currentLastName = currentAuthor.getElementsByTagName("LastName")[0].childNodes[0].data
					listFirstName = currentAuthor.getElementsByTagName("ForeName")
					if len(listFirstName) == 0:
						listFirstName = currentAuthor.getElementsByTagName("FirstName")
					if len(listFirstName) > 0:
						currentForeName = listFirstName[0].childNodes[0].data
				
				# do a bit of cleaning
				#currentLastName = currentLastName.encode("utf-8").replace("-", "").replace(" ", "") .replace("'", "")
				#currentForeName = currentForeName.encode("utf-8").replace("-", "").replace(" ", "") .replace("'", "")
				authors.append([currentLastName, currentForeName])
					
	#print "\nMajor MeSH terms:"
	meshTerms = []
	currentMeshTermsListList = article.getElementsByTagName("MeshHeadingList")
	if len(currentMeshTermsListList) > 0:
		for currentMeshTermsList in currentMeshTermsListList:
			for currentMeshDescriptor in currentMeshTermsList.getElementsByTagName("DescriptorName"):
				if "MajorTopicYN" in currentMeshDescriptor.attributes.keys() and currentMeshDescriptor.attributes["MajorTopicYN"].value == "Y":
					#print currentMeshDescriptor.childNodes[0].data.replace(" ", "").replace(",", "")
					meshTerms.append(currentMeshDescriptor.childNodes[0].data.replace(" ", "").replace(",", ""))





# print bibtex to stdout


#articleTemplate = """@Article{""" + authors[0][0].lower() + articleYear[-2:] + """,
#articleTemplate = """@Article{""" + authors[0][0].lower() + articleYear + """,
if len(authors) == 0:
	articleTemplate = """@Article{anonymous""" + articleYear + """,
  author =       {anonymous"""
else:
	articleTemplate = """@Article{""" + camelCase(authors[0][0]) + articleYear + """,
  author =       {"""
	for currentAuthor in authors[:-1]:
		if currentAuthor[1] != "":
			articleTemplate += currentAuthor[0] + ", " + currentAuthor[1] + " and "
		else:
			articleTemplate += currentAuthor[0] + " and "
	currentAuthor = authors[-1]
	if currentAuthor[1] != "":
		articleTemplate += currentAuthor[0] + ", " + currentAuthor[1]
	else:
		articleTemplate += currentAuthor[0]
	
articleTemplate += """},
  title =        {""" + articleTitle + """},
  journal =      {""" + journalTitle + """},
  year =         {""" + articleYear + """},
  OPTkey =       {},
  volume =       {""" + journalVolume + """},
"""
if journalNumber != "":
	articleTemplate += """  number =       {""" + journalNumber + """},
"""
else:
	articleTemplate += """  OPTnumber =    {""" + journalNumber + """},
"""
if len(articlePages) > 0:
	articleTemplate += """  pages =        {""" + articlePages + """},"""
else:
	articleTemplate += """  OPTpages =     {},"""
articleTemplate += """
  OPTmonth =     {},
"""
if len(articlePages) > 0:
	articleTemplate += """  OPTnote =      {},"""
else:
	articleTemplate += """  note =         {In press},"""
articleTemplate += """
  OPTannote =    {},
  OPTseeAlso =   {},
  PMID =         {""" + pmid + """},
"""
if articleDOI != "":
	articleTemplate += """  DOI =          {""" + articleDOI + """},
"""
articleTemplate += """  keyword =      {"""
if len(meshTerms) > 0:
	for currentMeshTerm in meshTerms[:-1]:
		articleTemplate += currentMeshTerm + ", "
	articleTemplate += meshTerms[-1]
articleTemplate += """}
}
"""

print("")
if len(authors) == 0:
	print("%%%%% anonymous" + articleYear + "_" + pmid + ".pdf")
else:
	print("%%%%% " + camelCase(authors[0][0]) + articleYear + "_" + pmid + ".pdf")
print(articleTemplate)
pubmedFile.close()
